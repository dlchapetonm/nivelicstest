export class Friend {
  id: number;
  firstName: string;
  lastName: String;
  phone: number;
}
