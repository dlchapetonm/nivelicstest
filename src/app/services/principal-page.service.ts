import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PrincipalPage } from '../models/principal-page';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PrincipalPageService {

  constructor(private client: HttpClient) { }

  getPrincipalPage(): Observable<PrincipalPage>{
    return this.client.get(environment.urlPrincipalPageService).pipe(
      map((x:any) => x.result)
    );
  }
}
