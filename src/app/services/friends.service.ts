import { Injectable } from '@angular/core';
import { Friend } from '../models/friend';

@Injectable({
  providedIn: 'root'
})
export class FriendsService {

  private friends: Friend[] = [];

  constructor() { }

  addFriend(friend: Friend){
    var id = 0;
    this.friends.forEach(item => {
      id = item.id > id? item.id: id;
    });

    friend.id = id + 1;

    this.friends.push(friend);
  }

  getFriends(): Friend[]{
    return this.friends;
  }

  editFriend(friend: Friend){
    for (let index = 0; index < this.friends.length; index++) {
      if(this.friends[index].id == friend.id){
        this.friends[index] = friend;
        return;
      }
    }
  }

  removeFriend(id: number){
    for (let index = 0; index < this.friends.length; index++) {
      if(this.friends[index].id == id){
        this.friends.splice(index, 1);
        return;
      }
    }
  }
}
