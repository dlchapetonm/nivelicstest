import { Component, OnInit } from '@angular/core';
import { PrincipalPageService } from 'src/app/services/principal-page.service';
import { PrincipalPage } from 'src/app/models/principal-page';
import { Category } from 'src/app/models/category';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.sass']
})
export class PrincipalComponent implements OnInit {

  public data: PrincipalPage;
  public categorySelect: Category;

  constructor(private principalService: PrincipalPageService) { }

  ngOnInit() {
    this.getData();
    this.categorySelect = null;
  }

  getData(){
    this.principalService.getPrincipalPage().subscribe(
      response => this.data = response
    );
  }

  selectCategory(category: Category){
    this.categorySelect = category;
  }

}
