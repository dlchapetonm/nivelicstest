import { Component, OnInit } from '@angular/core';
import { Friend } from 'src/app/models/friend';
import { FriendsService } from 'src/app/services/friends.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.sass']
})
export class FriendsComponent implements OnInit {

  public friends: Friend[]=[];
  public model: Friend;

  constructor(private friendService: FriendsService) { }

  ngOnInit() {
    this.getFriends();
    this.model = new Friend();
  }

  getFriends(){
    this.friends = this.friendService.getFriends();
  }

  saveFriend(){
    if(this.model.id == null){
      this.friendService.addFriend(this.model);
    }else{
      this.friendService.editFriend(this.model);
    }
    
    this.model = new Friend();
    this.getFriends();
  }

  removeFriend(friend: Friend){
    this.friendService.removeFriend(friend.id);
    this.getFriends();
  }

  editFriend(friend: Friend){
    this.model = Object.assign({}, friend);
  }

}
